// Define the inventory items
let inventory = [
  { id: 1, name: 'Product 1', quantity: 10, price: 5.99 },
  { id: 2, name: 'Product 2', quantity: 5, price: 9.99 },
  { id: 3, name: 'Product 3', quantity: 2, price: 14.99 }
];

// Function to render the inventory table
function renderInventoryTable() {
  let tbody = document.getElementById('inventory-table-body');
  tbody.innerHTML = '';
  inventory.forEach(item => {
    let tr = document.createElement('tr');
    tr.innerHTML = `
      <td>${item.id}</td>
      <td>${item.name}</td>
      <td>${item.quantity}</td>
      <td>$${item.price.toFixed(2)}</td>
      <td><button onclick="deleteItem(${item.id})">Delete</button></td>
    `;
    tbody.appendChild(tr);
  });
}

// Call the renderInventoryTable function to render the initial table
renderInventoryTable();

// Add event listener to the add item form
let addItemForm = document.getElementById('add-item-form');
addItemForm.addEventListener('submit', event => {
  event.preventDefault();
  let nameInput = document.getElementById('name-input');
  let quantityInput = document.getElementById('quantity-input');
  let priceInput = document.getElementById('price-input');
  let newItem = {
    id: inventory.length + 1,
    name: nameInput.value,
    quantity: parseInt(quantityInput.value),
    price: parseFloat(priceInput.value)
  };
  inventory.push(newItem);
  renderInventoryTable();
  addItemForm.reset();
});

// Function to delete an item from the inventory
function deleteItem(id) {
  inventory = inventory.filter(item => item.id !== id);
  renderInventoryTable();
}
